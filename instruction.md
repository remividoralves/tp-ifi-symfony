### installation sous linux de symfony
    wget https://get.symfony.com/cli/installer -O - | bash
### installation sous Windows de symfony
    Veuillez telecharger Symfony avec cette adresse
### Requirements
    Installer Php ==> sudo apt-get install php libapache2-mod-php
    Avoir une base de donée (Mysql ....)
    Un Ide
    Installer composer ==> https://getcomposer.org/download/
### Creer un nouveau projet    
    symfony new nomduprojet  
### installer orm-pack et marker-bundle
    composer require symfony/orm-pack  
    composer require --dev symfony/maker-bundle
### Creer une base de donne  
    php bin/console doctrine:database:create  
### Lier la Database  
    php bin/console make:migration   
    php bin/console doctrine:migrations:migrate  
### Lancer l'app
    symfony server:start        
### Arreter l'app
    symfony server:stop    


    

