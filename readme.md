**# Plan**  
    9->9:20 : presentation  
    9:20->9:30 : livecoding (Lucky coding - Palindrome)
    9:30-10:00 : TP PARTIE 1 CRUD
    10:05-10:15 : Correction
    10:15:10:30 : BREAK  
    10:30->11:45: TP PARTIE 2 FORUM
    11:45:12h : Correction  

**## Livecoding**  
    creation d'une page helloword lucky number et Palindrome
    Application lucky number  
        number100  
            route : config/routes.yaml  
            response : returning HTML from the controller  
        number  
            argument  
            route : annotation  
                Install the annotations package  
                composer require annotations  
            response : render template  
            Installer le paquet de Twig  
                composer require twig   
         Palindrome                 
**## Enoncé**   
    LE TP se compose en :  
    partie 1 : CRUD   
    partie 2 : FORUM   
